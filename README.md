# Driehoek van Pascal

## Ons idee
Voor onze GIP-opdracht moeten wij met javascript een animatie maken dat iets wetenschappelijks bevat. Wij willen een Driehoek van Pascal maken op basis van het aantal rijen dat een persoon op voorhand ingeeft.
Wanneer de persoon in kwestie op een knop drukt, zullen de bollen die op het scherm staan, een voor een verdwijnen en zal de driehoek getoond worden.

We werken met GSAP en javascript om dit te laten werken. Wanneer we tijdig klaar zijn, zullen we het ook proberen responsive te maken zodat het op eender welk toesten te zien is.

## Hoe ver zitten we?
We zijn al ver gevoderderd. Door een mergefail werkt onze GSAP jammer genoeg niet meer. We proberen dit zo snel mogelijk te maken, zodat we verder kunnen gaan met de afwerking hiervan.
De driehoek is bijna volledig af, zodat deze getoond kan worden. Het is dan enkel nog afwerken met css om het op de juiste positie te krijgen.

We hebben veel werk gehad om ons te verdiepen in de berekening van de Driehoek van Pascal. Ook GSAP was niet gemakkelijk, maar hier waren genoeg tutorials van om ons verder te helpen.
We hopen dat het onderzoekswerk dat we op voorhand hebben moeten doen en het werk voor de site zelf, te zien is.

### Een samenwerking tussen Ricardo Coenen en Maarten Vissers
